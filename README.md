# Pablo::Glob

TODO: Write a gem description

## Installation

Add this line to your application's Gemfile:

    gem 'Pablo-Glob'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install Pablo-Glob

## Usage

TODO: Write usage instructions here

### Validators

You can validate the presence of translations using TranslationPresenceValidator. The validator will make sure that the attribute
is translated at least in the primary language.

In order to validate the attribute `name` add the following line to your model class:

	validates_with Pablo::Glob::Validators::TranslationPresenceValidator, :attributes => :name

## TODO

* Create migration task for writing a migration and initializer
* Store translations in memory
* Create migration task which moves all un-translated messages to translated

## Contributing

1. Fork it ( http://github.com/<my-github-username>/Pablo-Glob/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
