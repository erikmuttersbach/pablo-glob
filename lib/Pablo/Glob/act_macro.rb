module Pablo
  module Glob
    module ActMacro
      def trans(*attrs)
        Pablo::Glob.init unless Pablo::Glob.init?

        setup_translations
        translate_attributes attrs
      end

      def setup_translations
        # Setup class attributes
        class_attribute :translated_attribute_names
        self.translated_attribute_names = []
      end

      def translate_attributes(attr_names)
        attr_names.each do |attr_name|
          # Add attribute to the list.
          self.translated_attribute_names << attr_name

          define_method(:"#{attr_name}=") do |value|
            if value.class == String
              value = {Pablo::Glob.config.primary_language => value}
            end
            write_attribute(attr_name.to_s, value ? value.to_json : nil)
          end

          define_method(attr_name.to_s) do
            value = read_attribute(attr_name.to_s)
            value = value && !value.empty? ? JSON.parse(value) : {}
            value.symbolize_keys
          end
        end
      end
    end
  end
end