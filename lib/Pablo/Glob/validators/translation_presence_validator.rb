require 'active_model/validator'

module Pablo
  module Glob
    module Validators
      class TranslationPresenceValidator < ActiveModel::EachValidator
        def validate_each(record, attribute, value)
          primary_visitor_class = Glob.config.primary_visitor_class
          if Thread.current[:glob] && Thread.current[:glob][:primary_visitor_class]
            primary_visitor_class = Thread.current[:glob][:primary_visitor_class]
          end
          if !value || !value[Glob.config.primary_language] ||
              (value[Glob.config.primary_language].is_a?(Hash) && (!value[Glob.config.primary_language][primary_visitor_class.to_s] || value[Glob.config.primary_language][primary_visitor_class.to_s] == ""))
            record.errors.add(attribute, :blank, options)
          end
        end
      end
    end
  end
end