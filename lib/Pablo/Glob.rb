require 'Pablo/Glob/version'

require 'active_support/configurable'

require 'pablo/glob/validators/translation_presence_validator'

module Pablo
  module Glob
    autoload :ActMacro,                     'pablo/glob/act_macro'
    autoload :InstanceMethods,              'pablo/glob/instance_methods'

    include ActiveSupport::Configurable

    @@init = false
    #backend = nil

    class << self
      def setup
        config.primary_language = :en

        yield config
      end

      attr_accessor :backend
    end

    def self.init
      @@init = true
    end

    def self.init?
      @@init
    end

  end
end

ActiveRecord::Base.extend(Pablo::Glob::ActMacro)
