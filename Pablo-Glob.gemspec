# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'Pablo/Glob/version'

Gem::Specification.new do |spec|
  spec.name          = 'pablo-glob'
  spec.version       = Pablo::Glob::VERSION
  spec.authors       = ['Erik Muttersbach']
  spec.email         = ['em@pabloguide.de']
  spec.summary       = %q{Write a short summary. Required.}
  spec.description   = %q{Write a longer description. Optional.}
  spec.homepage      = 'http://developer.pabloguide.de'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.5'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'database_cleaner'
  spec.add_development_dependency 'sqlite3'
  spec.add_development_dependency 'activerecord'
  spec.add_development_dependency 'activemodel'
  spec.add_development_dependency 'rspec', '~> 3.0.0.beta'
  spec.add_development_dependency 'rspec-core', '~> 3.0.0.beta'
  spec.add_development_dependency 'rspec-support', '~> 3.0.0.beta'
end
