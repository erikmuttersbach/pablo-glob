require 'spec_helper'

describe Item do
  before(:each) do
    Pablo::Glob.config.primary_language = :de
  end

  it 'lets me debug this text ;)' do
    @item = Item.new name: {:de => 'Mein Name', :en => 'My Name'}
    @item.save!
    expect(@item.name).to match({:de => 'Mein Name', :en => 'My Name'})
  end
end
