require 'spec_helper'

# TODO configure glob

describe Item do
  it 'has translated_attributes' do
    item = Item.new
    expect(Item.translated_attribute_names).to match_array([:name])
  end

  it 'can be created with "create"' do
    item = Item.create
    item.name = {:de => 'Mein Name', :en => 'My Name'}
    item.foo = 'test'
    item.save!
    item.reload
    expect(item.name).to match({:de => 'Mein Name', :en => 'My Name'})
  end

  it 'can be created with "new"' do
    item = Item.new
    item.name = {:de => 'Mein Name', :en => 'My Name'}
    item.save!
    item.reload
    expect(item.name).to match({:de => 'Mein Name', :en => 'My Name'})
  end

  it 'can be created with "create" passing a hash' do
    item = Item.create name: {:de => 'Mein Name', :en => 'My Name'}
    item.reload
    expect(item.name).to match({:de => 'Mein Name', :en => 'My Name'})
  end

  it 'can be created with "new" passing a hash' do
    item = Item.new name: {:de => 'Mein Name', :en => 'My Name'}
    item.save!
    item.reload
    expect(item.name).to match({:de => 'Mein Name', :en => 'My Name'})
  end

  it 'stores translations without saving them' do
    item = Item.new name: {:de => 'Mein Name', :en => 'My Name'}
    expect(item.name).to match({:de => 'Mein Name', :en => 'My Name'})
  end

  it 'can set a translated attribute using only the string' do
    item = Item.new name: 'My Name'
    expect(item.name).to match({:de => 'My Name'})
  end

  context 'which is created and saved' do
    before(:each) do
      @item = Item.create name: {:de => 'Mein Name', :en => 'My Name'}
      @item2 = Item.create name: {:de => 'Haus', :en => 'House'}
    end

    it 'can update translations' do
      @item.update_attributes! name: {de: 'Hallo', en: 'Hi'}
      @item.reload
      expect(@item.name).to match({:de => 'Hallo', :en => 'Hi'})
    end

    it 'can remove a translation' do
      @item.update_attributes! name: nil
      @item.reload
      expect(@item.name).to match({})
    end

    it 'stores the primary language in the original table' do
      names = ActiveRecord::Base.connection.execute("SELECT name FROM items WHERE id = #{@item.id}")
      expect(names[0]["name"]).to match "My Name"
    end

    it 'has no side effects when updating' do

    end
  end
end
