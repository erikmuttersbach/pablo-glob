# Setup bundler gems and require all
require 'bundler'
Bundler.setup(:default, :development)
Bundler.require(:development)

require 'active_record'
require 'active_support'

require 'Pablo/Glob'

# DB Setup
ActiveRecord::Base.establish_connection(
    :adapter => 'sqlite3',
    :database => ':memory'
)

# Require all supporting files
Dir['spec/support/**/*.rb'].each { |f| require f.gsub('spec/', '') }

RSpec.configure do |config|

  config.before(:suite) do
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)

    # Setup the Database
    ActiveRecord::Base.establish_connection(:adapter => "sqlite3",
                                            :database => ':memory')
  end

  config.mock_with :rspec do |c|
    c.syntax = [:should, :expect]
  end

  #config.order = "random"

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end
end
