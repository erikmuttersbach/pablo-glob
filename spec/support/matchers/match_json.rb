require 'rspec/expectations'

RSpec::Matchers.define :match_json do |expected|
  match do |actual|
    expected = JSON.parse(expected.to_json)
    RSpec::Support::FuzzyMatcher.values_match?(expected, actual)
  end
end