ActiveRecord::Schema.define do
  self.verbose = false

  create_table :items, :force => true do |t|
    t.string :name
    t.string :foo
    t.string :bar
  end

  create_table :translations, :force => true, :id => false do |t|
    t.string :klass
    t.string :attr
    t.integer :id
    t.string :lang
    t.string :value
  end

end

class Item < ActiveRecord::Base
  trans :name
end
